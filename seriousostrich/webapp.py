from datetime import time
import json
import os.path
import re
import tornado.ioloop
from tornado.log import enable_pretty_logging, app_log
import tornado.web

from .rules import BlockingRule, init as init_db, select_active_rules
from .hosts_file import rewrite_with_blocking
from .quotes import make_quotes_app

PORT = 8907
_PKGDIR = os.path.dirname(__file__)

class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.render("index.html")

_time_pattern = re.compile(r'(?P<hour>\d{1,2})[:.]?(?P<min>\d{2})')
def parse_time(s):
    m = _time_pattern.match(s)
    if not m:
        raise ValueError("Bad time input: %r" % s)
    h, m = m.group('hour', 'min')
    return time(hour=int(h), minute=int(m))

class RulesHandler(tornado.web.RequestHandler):
    def post(self):
        new_rule = BlockingRule.create(
            domain=self.get_body_argument('domain'),
            start=parse_time(self.get_body_argument('start')),
            end=parse_time(self.get_body_argument('end')),
        )
        update_blocking()
        self.set_status(201)
        self.finish(json.dumps(new_rule.to_jsonable_dict()))

    def get(self):
        rules = [r.to_jsonable_dict() for r in BlockingRule.select()\
                                        .order_by(BlockingRule.id.desc())]
        self.write(json.dumps(rules))

class RuleHandler(tornado.web.RequestHandler):
    def delete(self, id):
        BlockingRule.delete().where(BlockingRule.id == int(id)).execute()
        self.set_status(204)

def make_app():
    return tornado.web.Application([
        (r"/", MainHandler),
        (r"/rules", RulesHandler),
        (r"/rules/(?P<id>\d+)", RuleHandler),
    ], static_path=os.path.join(_PKGDIR, 'static'),
    compiled_template_cache=False,
    )

def prefix_www(domains):
    """Make a set of domains, including www. prefixed variants of non-www domains
    """
    res = set()
    for d in domains:
        res.add(d)
        if not d.startswith('www.'):
            res.add('www.' + d)
    return res

def update_blocking():
    domains = prefix_www(r.domain for r in select_active_rules())
    app_log.info("Updating hosts file, blocking %d domains" % len(domains))
    rewrite_with_blocking(sorted(domains))

def main():
    enable_pretty_logging()
    init_db()
    app = make_app()
    app.listen(PORT, address='localhost')

    regular_update = tornado.ioloop.PeriodicCallback(update_blocking, 60000)
    regular_update.start()

    quotes_app = make_quotes_app()
    try:
        quotes_app.listen(80, address='localhost')
    except PermissionError:
        app_log.info("Quotes pages will not be served")

    try:
        tornado.ioloop.IOLoop.current().start()
    except KeyboardInterrupt:
        print('Interrupted, quitting.')

if __name__ == "__main__":
    main()
