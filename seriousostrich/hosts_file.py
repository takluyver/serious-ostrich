import os
from pathlib import Path
import sys

if sys.platform == 'win32':
    SystemRoot = os.environ.get('SystemRoot', 'C:\\Windows')
    HOSTS_FILE = Path(SystemRoot, 'System32\\drivers\\etc\\hosts')
else:
    HOSTS_FILE = Path('/etc/hosts')

SECTION_START_LINE = "# BEGIN SERIOUSOSTRICH BLOCKS SECTION ---\n"
SECTION_END_LINE   = "# END SERIOUSOSTRICH BLOCKS SECTION ---\n"

def rewrite_with_blocking(domains_to_block):
    with HOSTS_FILE.open('r') as f:
        contents = f.readlines()

    new_block_lines = ['127.0.0.1  {}\n'.format(d) for d in domains_to_block]

    try:
        section_start_ix = contents.index(SECTION_START_LINE)
    except ValueError:
        # Our section does not already exist
        contents.append(SECTION_START_LINE)
        contents.extend(new_block_lines)
        contents.append(SECTION_END_LINE)
    else:
        try:
            section_end_ix = contents.index(SECTION_END_LINE, section_start_ix)
        except ValueError:
            contents.append(SECTION_END_LINE)
            section_end_ix = -1

        contents[section_start_ix+1:section_end_ix] = new_block_lines

    with HOSTS_FILE.open('w') as f:
        f.writelines(contents)

def remove_all_blocking():
    with HOSTS_FILE.open('r') as f:
        contents = []
        in_section = False
        for line in f:
            if line == SECTION_START_LINE:
                in_section = True
            if not in_section:
                contents.append(line)
            if line == SECTION_END_LINE:
                in_section = False

    with HOSTS_FILE.open('w') as f:
        f.writelines(contents)
