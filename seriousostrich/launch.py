import errno
import sys
import tornado.ioloop
import webbrowser

from  .webapp import main, PORT

if sys.platform == 'win32':
    ADDRINUSE = (errno.WSAEADDRINUSE, errno.EADDRINUSE)
else:
    ADDRINUSE = (errno.EADDRINUSE,)

def open_in_browser():
    webbrowser.open_new_tab('http://localhost:%d/' % PORT)

def launch_in_browser():
    loop = tornado.ioloop.IOLoop.current()
    loop.call_later(0.1, open_in_browser)

    try:
        main()
    except OSError as e:
        if e.errno not in ADDRINUSE:
            raise

        # The ioloop won't have been started, so do this here
        open_in_browser()

if __name__ == '__main__':
    if '--no-browser' in sys.argv:
        main()
    else:
        launch_in_browser()
