import os.path
import random
import tornado.web

_PKGDIR = os.path.dirname(__file__)

quotes = [
    ("Never put off till tomorrow what may be done day after tomorrow just as well.", "Mark Twain"),
    ("Ayn Rand: how is she still a thing?", "Last Week Tonight"),
    ("Procrastination is the thief of time, collar him.", "Charles Dickens"),
    ("A day can really slip by when you're deliberately avoiding what you're supposed to do.", "Bill Watterson"),
    ("Procrastination is my sin. It brings me naught but sorrow. I know that I should stop it. In fact, I will―tomorrow.", "Gloria Pitzer"),
    ("I carried the sheep for you. I climbed the tree. I rode the back of the truck. But now I have to X-ray these geese.", "Martin"),
    ("I am not procrastinating, I am simply wisely waiting, in case the magic pixies come and do it in the night.", "John Finnemore's Souvenir Programme"),
]

class PageHandler(tornado.web.RequestHandler):
    def get(self, _):
        quotation, author = random.choice(quotes)
        self.render("quotepage.html", quotation=quotation, author=author)

def make_quotes_app():
    return tornado.web.Application([
        (r"/(.*)", PageHandler),
    ], static_path=os.path.join(_PKGDIR, 'static'),
    compiled_template_cache=False,
    )
