from datetime import datetime, time
import os
from pathlib import Path
import sys

import peewee

def find_data_dir():
    home = os.path.expanduser('~')

    if sys.platform == 'darwin':
        return Path(home, 'Library', 'Seriousostrich')
    elif os.name == 'nt':
        appdata = os.environ.get('APPDATA', None)
        if appdata:
            return Path(appdata, 'seriousostrich')
        else:
            return Path(home, '.seriousostrich')
    else:
        # Linux, non-OS X Unix, AIX, etc.
        xdg = os.environ.get("XDG_DATA_HOME", None)
        if xdg:
            return Path(xdg, 'seriousostrich')
        else:
            return Path(home, '.local', 'share', 'seriousostrich')

# File location is considered later
db = peewee.SqliteDatabase(None)

def init(db_file=None):
    if db_file is None:
        data_dir = find_data_dir()
        data_dir.mkdir(parents=True, exist_ok=True)
        db_file = data_dir / 'rules.sqlite'
    db.init(str(db_file))
    db.create_tables([BlockingRule], safe=True)


class BlockingRule(peewee.Model):
    domain = peewee.TextField()
    start = peewee.TimeField()
    end = peewee.TimeField()

    class Meta:
        database=db

    def to_jsonable_dict(self):
        return {
            'id': self.id,
            'domain': self.domain,
            'start': self.start.isoformat(),
            'end': self.end.isoformat(),
        }

def select_active_rules(time=None):
    if time is None:
        time = datetime.now().time()

    normal_include = (BlockingRule.start <= BlockingRule.end) & \
                     (BlockingRule.start <= time) & (time < BlockingRule.end)
    # Rules that wrap around midnight have end time < start time
    wrap_include = (BlockingRule.start > BlockingRule.end) & \
                   ((BlockingRule.start <= time) | (time < BlockingRule.end))

    return BlockingRule.select().where(normal_include | wrap_include)
