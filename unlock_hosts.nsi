[% extends "pyapp_msvcrt.nsi" %]

[# This is just a convenient block to add an extra command to the install section #]
[% block install_shortcuts %]
[[ super() ]]
    DetailPrint "Unlocking $SYSDIR\drivers\etc\hosts"
    AccessControl::GrantOnFile \
        "$SYSDIR\drivers\etc\hosts" "(BU)" "GenericRead + GenericWrite"
    Pop $0
[% endblock install_shortcuts %]
