#!/bin/bash
set -euo pipefail

if [[ $EUID -ne 0 ]]; then
   echo "Please run with sudo (Serious Ostrich needs to be installed as root)"
   exit 1
fi

ENV_DIR=/opt/serious-ostrich/env

echo "Creating Python environment..."
python3 -m venv --clear $ENV_DIR

echo "Installing packages..."
$ENV_DIR/bin/pip install tornado~=6.1 peewee~=3.14 .

echo "Setting up systemd service..."
cp serious-ostrich.service /etc/systemd/system/serious-ostrich.service
systemctl enable --now serious-ostrich.service

echo "Done. To check status:"
echo "  systemctl status serious-ostrich"
echo "To use, visit: http://localhost:8907/"
