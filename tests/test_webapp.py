from datetime import time

from seriousostrich import webapp


def test_parse_time():
    assert webapp.parse_time('9:00') == time(9, 0)
    assert webapp.parse_time('17.00') == time(17, 0)
    assert webapp.parse_time('0200') == time(2, 0)

def test_prefix_www():
    assert webapp.prefix_www(['facebook.com', 'www.twitter.com']) == \
           {'facebook.com', 'www.facebook.com', 'www.twitter.com'}
