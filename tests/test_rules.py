from datetime import time

from seriousostrich import rules

br = rules.BlockingRule

def domains(query):
    return {rule.domain for rule in query}

def test_select_active_rules():
    rules.init(':memory:')

    br.create(domain='facebook.com', start=time(9, 0), end=time(17,0))
    br.create(domain='twitter.com', start=time(10, 0), end=time(17,0))
    br.create(domain='buzzfeed.com', start=time(21, 0), end=time(3, 0))

    sar = rules.select_active_rules
    assert domains(sar(time(12, 0))) == {'facebook.com', 'twitter.com'}
    assert domains(sar(time(9, 30))) == {'facebook.com'}
    assert domains(sar(time(18, 30))) == set()
    assert domains(sar(time(22, 0))) == {'buzzfeed.com'}
    assert domains(sar(time(2, 0))) == {'buzzfeed.com'}
